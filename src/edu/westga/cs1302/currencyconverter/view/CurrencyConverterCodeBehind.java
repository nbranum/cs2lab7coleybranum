package edu.westga.cs1302.currencyconverter.view;


import edu.westga.cs1302.currencyconverter.viewmodel.CurrencyConverterViewModel;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.util.converter.NumberStringConverter;

/**
 * the currency converter code behind class.
 * 
 * @author nbranum1
 *
 */
public class CurrencyConverterCodeBehind {
	
    @FXML
    private Label errorMessageLabel;

    @FXML
    private TextArea resultTextArea;

    @FXML
    private TextField amountTextField;

    @FXML
    private TextField targetCurrencyTextField;

    
    private CurrencyConverterViewModel viewModel;
    
    /**
     * Instantiates a new currency converter code behind.
     * 
     * @precondition none
     * @postcondition none
     * 
     */
    public CurrencyConverterCodeBehind() {
    	this.viewModel = new CurrencyConverterViewModel();
    }
    
    @FXML
    private void initialize() {
    	this.resultTextArea.setEditable(false);
    	this.resultTextArea.setFont(javafx.scene.text.Font.font("Courier", FontWeight.NORMAL, FontPosture.REGULAR, 13));
    	this.errorMessageLabel.textProperty().bindBidirectional(this.viewModel.getErrorMessageProperty());
    	this.targetCurrencyTextField.textProperty().bindBidirectional(this.viewModel.getTargetCurrencyProperty());
    	this.amountTextField.textProperty().bindBidirectional(this.viewModel.getAmountProperty(), new NumberStringConverter());
    	this.resultTextArea.textProperty().bindBidirectional(this.viewModel.getResultProperty());
    }
    
    @FXML
    void handleConvertAmount() {
    	this.viewModel.convertAmount();
    }
    
}
