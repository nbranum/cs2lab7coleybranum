package edu.westga.cs1302.currencyconverter.viewmodel;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import edu.westga.cs1302.currencyconverter.model.ConversionRate;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * the currency converter view model class.
 * 
 * @author nbranum1
 *
 */
public class CurrencyConverterViewModel {

	private DoubleProperty amountProperty;
	private StringProperty targetCurrencyProperty;
	private StringProperty resultProperty;
	private StringProperty errorMessageProperty;
	public static final String ERROR_MESSAGE = "Invalid currency, use the 3-letter codes.";

	/**
	 * Instantiates a new viewModel.
	 */
	public CurrencyConverterViewModel() {
		this.amountProperty = new SimpleDoubleProperty();
		this.targetCurrencyProperty = new SimpleStringProperty();
		this.resultProperty = new SimpleStringProperty();
		this.errorMessageProperty = new SimpleStringProperty();
	}

	/**
	 * gets the amount property.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return DoubleProperty that is the amount property
	 */
	public DoubleProperty getAmountProperty() {
		return this.amountProperty;
	}

	/**
	 * gets the target currency property.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return StringProperty that is the target currency property
	 */
	public StringProperty getTargetCurrencyProperty() {
		return this.targetCurrencyProperty;
	}

	/**
	 * gets the result property.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return StringProperty that is the result property
	 */
	public StringProperty getResultProperty() {
		return this.resultProperty;
	}

	/**
	 * gets the error message property.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return StringProperty that is the error message property
	 */
	public StringProperty getErrorMessageProperty() {
		return this.errorMessageProperty;
	}

	/**
	 * checks the validity of the entered conversion code.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return double value if code is valid, else null
	 */
	public ConversionRate checkCode() {
		for (ConversionRate conversionRate : ConversionRate.values()) {
			if (conversionRate.toString().equals(this.targetCurrencyProperty.getValue())) {
				return conversionRate;
			}
		}
		return null;
	}

	/**
	 * converts the amount based on the specified conversion.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 */
	public void convertAmount() {
		if (this.checkCode() == null) {
			this.errorMessageProperty.setValue(ERROR_MESSAGE);
		} else {
			this.errorMessageProperty.set("");
			Double convertedAmount = this.checkCode().exchange(this.getAmountProperty().doubleValue());
			DecimalFormat formatter = new DecimalFormat();
			formatter.applyPattern("#,###.##");
			formatter.setRoundingMode(RoundingMode.FLOOR);
			String convertedAmountFormatted = formatter.format(convertedAmount);
			String result = "$" + this.getAmountProperty().doubleValue() + " = " + convertedAmountFormatted + " "
					+ this.getTargetCurrencyProperty().getValue();
			this.resultProperty.set(result);
		}
	}

}
