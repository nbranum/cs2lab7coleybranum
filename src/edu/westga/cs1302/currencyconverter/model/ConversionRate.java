package edu.westga.cs1302.currencyconverter.model;

/**
 * Enum ConversionRates.
 * 
 * @author CS1302
 */
public enum ConversionRate {

	AUD(1.49484), 
	CAD(1.32360), 
	CNY(6.93664), 
	EUR(0.90120), 
	GBP(0.75727), 
	INR(71.5310), 
	JPY(120.235);

	private final double conversionFactor;

	/**
	 * Creates conversion rate with the specified factor.
	 * 
	 * @param factor
	 */
	ConversionRate(double factor) {
		this.conversionFactor = factor;
	}

	/**
	 * Returns the amount exchanged to this currency.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param amount the amount
	 * @return the exchanged amount
	 */
	public double exchange(double amount) {
		return this.conversionFactor * amount;
	}
}
